package ebin.image;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class ImageLoaderControler {
    private static ImageLoaderControler ourInstance;
    Context ct;
    Boolean loadParallel;
    private LruCache mMemoryCache;

    public static ImageLoaderControler getInstance(Context ct, Boolean loadParallel, int percentOfRamAllocate) {
        if(ourInstance==null) ourInstance = new ImageLoaderControler(ct,  loadParallel, percentOfRamAllocate);
        return ourInstance;
    }

    private ImageLoaderControler(Context ct, Boolean loadParallel, int percentOfRamAllocate) {
         this.ct = ct;
         this.loadParallel = loadParallel;
        ActivityManager am = (ActivityManager) ct.getSystemService(
                Context.ACTIVITY_SERVICE);
        int maxKb = am.getMemoryClass() * 1024;
        // percentage of RAM allocate commonly 1 of 8
        int limitKb;
        if(percentOfRamAllocate<=80 && percentOfRamAllocate>10) limitKb = (int) (maxKb*(percentOfRamAllocate*.01)); else  limitKb = maxKb / 8;
        mMemoryCache = new LruCache(limitKb);

    }

    public AsyDrawable loadImageToImageView(Resources rs, int requiredImageHeight, int requiredImageWidth, ImageView imageView, String url, String urlKeyForBitmapForOrientation){
                DownloadTask downloadtask = new DownloadTask(url, imageView, requiredImageHeight , requiredImageWidth,  mMemoryCache, urlKeyForBitmapForOrientation);
        if(loadParallel)downloadtask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else downloadtask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        AsyDrawable asyDrawable = new AsyDrawable(rs, null, downloadtask);
        return  asyDrawable;
    }




    public class AsyDrawable extends BitmapDrawable {
        WeakReference<DownloadTask> taskRefer ;

        AsyDrawable(Resources res,
                    Bitmap bmapPlace,
                    DownloadTask taskRef){
            super(res,bmapPlace);

            taskRefer = new WeakReference<DownloadTask>(taskRef);
        }
        public DownloadTask getBitMapTask(){
            return taskRefer.get();
        }
    }
    public static boolean checkBitmapWorkerTask (String imagesUrl,
                                                 ImageView imageView){
        DownloadTask bmWork = getBitmapWorkerTask(imageView);
        if(bmWork != null){
            final String workUrl = bmWork.getmImageFile();
            if(workUrl != null){
                bmWork.cancel(true);
            }else {
                // bitmap worker task file is same as imageView expecting.
                return  false;
            }
        }
        return  true;
    }

    public static DownloadTask getBitmapWorkerTask(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof AsyDrawable) {
            AsyDrawable asyDrawable = (AsyDrawable) drawable;
            return asyDrawable.getBitMapTask();
        }
        return null;
    }
}
