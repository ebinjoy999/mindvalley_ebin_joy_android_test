package ebin.image;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;



	public class DownloadTask extends AsyncTask<String, Bitmap, Bitmap> {

		String url, urlKeyForBitmapForOrientation;
		int imageHeight,  imageWidth;
		LruCache mMemoryCache;
		private final WeakReference<ImageView> imageViewReference;

		public DownloadTask(String url, ImageView imageview, int imageHeight, int imageWidth, LruCache mMemoryCache, String urlKeyForBitmapForOrientation ) {
			imageViewReference = new WeakReference<ImageView>(imageview);
			this.url = url;
			this.imageWidth = imageWidth;
			this.imageHeight = imageHeight;
            this.mMemoryCache = mMemoryCache;
			this.urlKeyForBitmapForOrientation = urlKeyForBitmapForOrientation;
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap bmp = null;
			if (mMemoryCache.get(url+urlKeyForBitmapForOrientation) == null){
				try {
					bmp = getBitmapFromUrl(url);
					return bmp;
				} catch (Exception e) {
					e.printStackTrace();
				}
		      }else {
				return (Bitmap) mMemoryCache.get(url+urlKeyForBitmapForOrientation);
			}
			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if(!isCancelled()) // Checking whether is a cancelled Task
			if (imageViewReference != null) {
				ImageView imageView = imageViewReference.get();
				if (imageView != null) {
					if(url!=null && result!=null){
						mMemoryCache.put(url+urlKeyForBitmapForOrientation,result);
						imageView.setImageBitmap(result);
					}
				}
			}
		}
		public String getmImageFile(){
			return  url;
		}




		public Bitmap getBitmapFromUrl(String src) throws Exception {
			Bitmap bmp = null;
			URL imageUrl = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) imageUrl
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			bmp = BitmapFactory.decodeStream(input);
			return getResizedBitmap(bmp,imageWidth,imageHeight);
		}

		public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

			if(bm!=null) {
				int width = bm.getWidth();
			int height = bm.getHeight();
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;
			// CREATE A MATRIX FOR THE MANIPULATION
			Matrix matrix = new Matrix();
			// RESIZE THE BIT MAP
			matrix.postScale(scaleWidth, scaleHeight);

			// "RECREATE" THE NEW BITMAP
			Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
					matrix, false);

			return resizedBitmap;
			}else {
				return  null;
			}

		}
	}



