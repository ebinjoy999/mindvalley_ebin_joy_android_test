package retrofit;


import java.util.List;

import ebin.ebinjoy_android_test.pinboard.model.Pin;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    //get a pins
    @GET("wgkJgazE")
    Call<List<Pin>> getSingleCheckin();
}