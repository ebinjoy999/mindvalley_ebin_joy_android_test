package ebin.ebinjoy_android_test;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ebin.ebinjoy_android_test.pinboard.model.Pin;
import ebin.ebinjoy_android_test.utils.ScreenSpecFetch;
import retrofit.ApiClient;
import retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;

public class PinBoardActivity extends AppCompatActivity implements PinRecyclerViewAdapter.ClickListenerPinRecyclerAdapter{

    private static final String TAG = "PinBoardActivity";
    private static int GRID_COUNT = 2;
    private int SCREEN_ORIENTATION ;
    RecyclerView recyclerViewPin;
    AppCompatButton buttonRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar;
    TextView tvFailed;
    private RecyclerView.LayoutManager layoutManager;
    List<Pin> pins;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_board);
        setupUI();
        loadPinToBoard();
        buttonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadPinToBoard();
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPinToBoard();
            }
        });

    }




    private void loadPinToBoard() {
        swipeRefreshLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        recyclerViewPin.setVisibility(View.GONE);
        tvFailed.setVisibility(View.GONE);
        buttonRetry.setVisibility(View.GONE);

        ApiInterface apiService =ApiClient.getClient().create(ApiInterface.class);
        Call<List<Pin>> call = apiService.getSingleCheckin();
        call.enqueue(new Callback<List<Pin>>() {

            @Override
            public void onResponse(Call<List<Pin>> call, retrofit2.Response<List<Pin>> response) {
              Log.e(TAG,response.body().size()+"");
               try{
                   if(response.body().size()>0){
                        setUpAdapterAndClick(response.body());
                   }else{
                      if(progressBar!=null){
                          setUpInfoAfterPinFetch(getResources().getString(R.string.no_pins_found),false);
                      }
                   }
               }catch (Exception exc){
                   exc.printStackTrace();
                   if(progressBar!=null) {
                       setUpInfoAfterPinFetch(getResources().getString(R.string.server_response_invalid),true);
                   }
               }
            }

            @Override
            public void onFailure(Call<List<Pin>>call, Throwable t) {
                if(progressBar!=null){
                 setUpInfoAfterPinFetch(getResources().getString(R.string.cannot_connect),true);
                }
            }
        });
    }

   private void setUpInfoAfterPinFetch(String s, boolean isRetry){
       progressBar.setVisibility(View.GONE);
       recyclerViewPin.setVisibility(View.GONE);
       tvFailed.setVisibility(View.VISIBLE);
       if(isRetry)buttonRetry.setVisibility(View.VISIBLE); else buttonRetry.setVisibility(View.GONE);
       tvFailed.setText(s);
       onItemsLoadComplete();
    }

    private void setUpAdapterAndClick(List<Pin> body) {
       if(recyclerViewPin!=null) {
           this.pins = body;

           SCREEN_ORIENTATION = ScreenSpecFetch.getScreenOrientation(PinBoardActivity.this);
           if(SCREEN_ORIENTATION == Configuration.ORIENTATION_LANDSCAPE) this.GRID_COUNT = 3; else  this.GRID_COUNT = 2;

           progressBar.setVisibility(View.GONE);
           recyclerViewPin.setVisibility(View.VISIBLE);
           tvFailed.setVisibility(View.GONE);
           buttonRetry.setVisibility(View.GONE);
           swipeRefreshLayout.setVisibility(View.VISIBLE);
           onItemsLoadComplete();
           PinRecyclerViewAdapter pinAdapter = new PinRecyclerViewAdapter(body,GRID_COUNT, PinBoardActivity.this);
           pinAdapter.setUpClickListener(PinBoardActivity.this);
           recyclerViewPin.setClickable(true);
           recyclerViewPin.setHasFixedSize(true);
           layoutManager = new StaggeredGridLayoutManager(GRID_COUNT, StaggeredGridLayoutManager.VERTICAL);
           recyclerViewPin.setLayoutManager(layoutManager);;
           recyclerViewPin.setAdapter(pinAdapter);
       }
    }


    private void setupUI() {
        recyclerViewPin = (RecyclerView) findViewById(R.id.rv_pin);
        buttonRetry = (AppCompatButton) findViewById(R.id.btn_retry);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        tvFailed = (TextView) findViewById(R.id.tv_failed);

//        recyclerViewPin.setItemAnimator(new FadingItemAnimator());
    }

    void onItemsLoadComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }
    @Override
    public void getClickOfPinRecyclerAdapter(int position) {
        // handle Click
    }


}
