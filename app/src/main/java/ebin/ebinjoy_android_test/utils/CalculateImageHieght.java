package ebin.ebinjoy_android_test.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import ebin.ebinjoy_android_test.R;
import retrofit2.http.POST;


public class CalculateImageHieght {

    int screenWidth;
    Context c;
    boolean PORT;

    private static CalculateImageHieght ourInstance;

    public static CalculateImageHieght getInstance(Context c) {
       if(ourInstance==null) ourInstance = new CalculateImageHieght(c);
        return ourInstance;
    }

    private CalculateImageHieght(Context c) {
        this.c = c;
    }

    public int getInvidualItemHieght(int height, int width, int GRID_COUNT){
        this.screenWidth = ScreenSpecFetch.calculateScreenWidth(c);
        int exceptPaddingScreenWidth = (screenWidth - (int) ScreenSpecFetch.convertDpToPixel(((GRID_COUNT*(c.getResources().getDimension(R.dimen.pinMargin)*2))+1),c));
        return  height / ( width / (exceptPaddingScreenWidth/GRID_COUNT) ) ;
    }
}
