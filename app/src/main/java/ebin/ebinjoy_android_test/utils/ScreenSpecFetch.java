package ebin.ebinjoy_android_test.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Surface;

public class ScreenSpecFetch {


    public static  int calculateScreenWidth(Context c){

        try{
            Display dis = ((Activity) c).getWindowManager().getDefaultDisplay();
            int width = dis.getWidth();
            int height = dis.getHeight();


            return  width;
        }catch (Exception ex){ //Things gone beyond the context
            return 0;
        }
    }


    public static float convertDpToPixel(float dp, Context context){
        try {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            float px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
            return px;
        }catch (Exception ex){ //Things gone beyond the context
            ex.printStackTrace();
        }
        return 0;
    }

    public static int getScreenOrientation(Context ct) {
        Display getOrient = ((Activity) ct).getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;

            if(getOrient.getWidth() < getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_PORTRAIT;
            }else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;

        }

        return orientation;
    }
}
