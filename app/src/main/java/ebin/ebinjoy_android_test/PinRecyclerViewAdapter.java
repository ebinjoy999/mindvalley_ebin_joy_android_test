package ebin.ebinjoy_android_test;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import deprication.DepricationCompact;
import ebin.ebinjoy_android_test.pinboard.model.Category;
import ebin.ebinjoy_android_test.pinboard.model.Pin;
import ebin.ebinjoy_android_test.utils.CalculateImageHieght;
import ebin.ebinjoy_android_test.utils.ScreenSpecFetch;
import ebin.image.ImageLoaderControler;

public class  PinRecyclerViewAdapter  extends RecyclerView.Adapter<PinRecyclerViewAdapter.MyViewHolder> {

    private List<Pin> pins;
    Context c;
    int GRID_COUNT;
    int PERCENTAGE_RAM_USE = 15;
    String urlKeyForBitmapForOrientation;

    ClickListenerPinRecyclerAdapter  clickListenerPinRecyclerAdapter;
    CalculateImageHieght calculateImageHieght;
    Typeface  robotoMedium, robotoRegular, robotoMediumItaics;
    ImageLoaderControler imageLoaderControler;
    int requiredImageWidth = 0;



    public void setUpClickListener(Context ct) {

     clickListenerPinRecyclerAdapter = (ClickListenerPinRecyclerAdapter) ct;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        ImageView customPinImageView, imageViewThumbUp;
        TextView textViewPinCategories, textViewUserName, textviewCustomUserLike;
        public MyViewHolder(View view) {
           super(view);
            customPinImageView = (ImageView) view.findViewById(R.id.customPinImageView);
            imageViewThumbUp =  (ImageView) view.findViewById(R.id.imageViewThumbUp);
            textViewPinCategories = (TextView) view.findViewById(R.id.textViewPinCategories);
            textViewUserName = (TextView) view.findViewById(R.id.textViewUserName);
            textviewCustomUserLike = (TextView) view.findViewById(R.id.textviewCustomUserLike);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
          if(clickListenerPinRecyclerAdapter!=null) clickListenerPinRecyclerAdapter.getClickOfPinRecyclerAdapter(getAdapterPosition());
        }
    }


    public PinRecyclerViewAdapter(List<Pin> pins,int GRID_COUNT, Context c) {
        this.pins = pins;
        this.c = c;
        this.GRID_COUNT = GRID_COUNT;
        calculateImageHieght =  CalculateImageHieght.getInstance(c);
        robotoMedium  = Typeface.createFromAsset(c.getAssets(), "fonts/Roboto-Medium.ttf");
        robotoRegular = Typeface.createFromAsset(c.getAssets(), "fonts/Roboto-Regular.ttf");
        robotoMediumItaics= Typeface.createFromAsset(c.getAssets(), "fonts/Roboto-MediumItalic.ttf");
        imageLoaderControler = imageLoaderControler.getInstance(c,false,PERCENTAGE_RAM_USE);
        int width =  (int) (ScreenSpecFetch.calculateScreenWidth(c)-(ScreenSpecFetch.convertDpToPixel(((GRID_COUNT*(c.getResources().getDimension(R.dimen.pinMargin)*2))+1),c)));
         this.requiredImageWidth = width/GRID_COUNT;
         urlKeyForBitmapForOrientation = (ScreenSpecFetch.getScreenOrientation(c)== Configuration.ORIENTATION_PORTRAIT)? "ORIENTATION_PORTRAIT": "ORIENTATION_LANDSCAPE";
    }

    @Override
    public PinRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.board_ustom_pin_item, parent, false);
        return new PinRecyclerViewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PinRecyclerViewAdapter.MyViewHolder holder, int position) {

        holder.customPinImageView.requestLayout();
        Pin pin = pins.get(position);

        holder.textViewUserName.setTypeface(robotoRegular);
        holder.textViewPinCategories.setTypeface(robotoMedium);
        holder.textviewCustomUserLike.setTypeface(robotoMediumItaics);

// set imageview height according to the Original image width and height
        int height = calculateImageHieght.getInvidualItemHieght(pin.getHeight(),pin.getWidth(),GRID_COUNT);
        if(pin.getHeight()!=null && pin.getWidth()!=null)holder.customPinImageView.getLayoutParams().height = height;

        if(pin.getColor()!=null)holder.customPinImageView.setBackgroundColor(Color.parseColor(pin.getColor()));
        if(pin.getLikedByUser()!=null)if(pin.getLikedByUser()) holder.imageViewThumbUp.setColorFilter(DepricationCompact.getColor(c,R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);;
        if(pin.getLikes()!=null)holder.textviewCustomUserLike.setText(pin.getLikes()+" Likes");
        if(pin.getUser().getName()!=null)holder.textViewUserName.setText(pin.getUser().getName());

        String stringCategory = "";
        for(Category cat: pin.getCategories()){
            stringCategory = stringCategory + cat.getTitle()+", ";
        }
        if(stringCategory.length()>2)holder.textViewPinCategories.setText(stringCategory.substring(0,stringCategory.length()-2));

//        Image Loader Loading Image
        int requiredImageHieght = holder.customPinImageView.getLayoutParams().height;
        if(imageLoaderControler.checkBitmapWorkerTask(pin.getUrls().getSmall(),holder.customPinImageView)){
            ImageLoaderControler.AsyDrawable asyDrawable =   imageLoaderControler.loadImageToImageView(holder.customPinImageView.getResources(),
                    requiredImageHieght,requiredImageWidth,
                    holder.customPinImageView,pin.getUrls().getSmall(),urlKeyForBitmapForOrientation);
            holder.customPinImageView.setImageDrawable(asyDrawable);
        }


    }

    @Override
    public int getItemCount() {
        return pins.size();
    }



    public interface ClickListenerPinRecyclerAdapter{
         void getClickOfPinRecyclerAdapter(int position);
    }
}